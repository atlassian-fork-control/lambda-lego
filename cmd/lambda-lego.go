package main

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"log"
	"strings"
	"time"

	"bitbucket.org/atlassian/lambda-lego/internal/cfg"
	"bitbucket.org/atlassian/lambda-lego/pkg/http01"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/acm"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/go-acme/lego/certcrypto"
	"github.com/go-acme/lego/certificate"
	"github.com/go-acme/lego/lego"
	"github.com/go-acme/lego/registration"
)

// LambdaUser : implementation of Acme User
type LambdaUser struct {
	Email        string
	Registration *registration.Resource
	key          crypto.PrivateKey
}

// GetEmail : gets the email of the Lambda Acme user
func (u *LambdaUser) GetEmail() string {
	return u.Email
}

// GetRegistration : gets the registration of the Lambda Acme user
func (u LambdaUser) GetRegistration() *registration.Resource {
	return u.Registration
}

// GetPrivateKey : gets the private key of the Lambda Acme user
func (u *LambdaUser) GetPrivateKey() crypto.PrivateKey {
	return u.key
}

// DoACME fills out the ACME protocol to request a certificate. It uploads the challenge response to s3 to be served by a different lambda
// When the challenge is complete, DoAcme uploads the certificate to ACM and attaches it to the load balancer
func DoACME(ctx context.Context, request events.ALBTargetGroupRequest) (events.ALBTargetGroupResponse, error) {
	llcfg := cfg.New()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(llcfg.GetRegion())},
	)
	if err != nil {
		log.Println("Error starting AWS session")
		log.Fatal(err)
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}

	privateKey, userRegistered := getOrCreatePrivateKey(llcfg, sess)

	user := LambdaUser{
		// Replace with env var
		Email: llcfg.GetAdminEmail(),
		key:   privateKey,
	}

	config := lego.NewConfig(&user)

	// Replace with env var
	log.Printf("Querying ACME api at: %s\n", llcfg.GetCertificateAuthorityEndpoint())
	config.CADirURL = llcfg.GetCertificateAuthorityEndpoint()
	config.Certificate.KeyType = certcrypto.RSA2048

	client, err := lego.NewClient(config)
	if err != nil {
		log.Println("Error creating client from config")
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}

	err = client.Challenge.SetHTTP01Provider(http01.NewProviderS3(llcfg.GetBucketName(), llcfg.GetAcmeChallengeS3Key(), llcfg.GetRegion()))
	if err != nil {
		log.Println("Unable to set challenge provider to s3 provider")
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}

	var reg *registration.Resource
	if userRegistered {
		log.Println("Geting registration by key")
		reg, err = client.Registration.ResolveAccountByKey()
		if err != nil {
			log.Println("Unable to retrieve existing registration for user")
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}
	} else {
		log.Println("Registering new ACME user")
		reg, err = client.Registration.Register(registration.RegisterOptions{TermsOfServiceAgreed: true})
		if err != nil {
			log.Println("Unable to register new ACME user")
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}
	}
	user.Registration = reg

	csr := certificate.ObtainRequest{
		Domains: []string{llcfg.GetDomain()},
		Bundle:  false,
	}

	if !llcfg.IsDryRun() {
		log.Println("Trying to obtain certificate")
		certificate, err := client.Certificate.Obtain(csr)
		if err != nil {
			log.Println("Unable to obtain certificate")
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}

		cert := strings.SplitAfter(string(certificate.Certificate), "-----END CERTIFICATE-----")[0]

		log.Println("Storing certificate in s3")
		s3sess := s3.New(sess)

		var tagBuilder strings.Builder
		tagBuilder.WriteString("IssuedAt=")
		tagBuilder.WriteString(time.Now().Format("01-02-2006 15:04:05"))
		tagBuilder.WriteString("&atlCertDomain=")
		tagBuilder.WriteString(llcfg.GetDomain())

		putInput := &s3.PutObjectInput{
			Body:    strings.NewReader(cert),
			Bucket:  aws.String(llcfg.GetBucketName()),
			Key:     aws.String(llcfg.GetCertificateS3Key()),
			Tagging: aws.String(tagBuilder.String()),
		}

		_, err = s3sess.PutObject(putInput)
		if err != nil {
			log.Println("Error storing certificate in s3")
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}

		log.Println("Uploading certificate to ACM")
		acmsess := acm.New(sess)
		acmImportInput := &acm.ImportCertificateInput{
			Certificate:      []byte(cert),
			CertificateChain: certificate.IssuerCertificate,
			PrivateKey:       certificate.PrivateKey,
		}

		importOutput, err := acmsess.ImportCertificate(acmImportInput)
		if err != nil {
			log.Println("Error importing certificate into ACM")
			log.Fatal(err)
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}

		certArn := importOutput.CertificateArn

		listenerCertificate := &elbv2.Certificate{
			CertificateArn: certArn,
		}
		listenerAction := &elbv2.Action{
			Type:           aws.String(elbv2.ActionTypeEnumForward),
			TargetGroupArn: aws.String(llcfg.GetClusterTargetGroup()),
		}

		log.Println("Attaching certificate to ALB")
		elbv2client := elbv2.New(sess)
		createListenerInput := &elbv2.CreateListenerInput{
			Certificates:    []*elbv2.Certificate{listenerCertificate},
			DefaultActions:  []*elbv2.Action{listenerAction},
			LoadBalancerArn: aws.String(llcfg.GetALB()),
			Port:            aws.Int64(llcfg.GetClusterHTTPSPort()),
			Protocol:        aws.String(elbv2.ProtocolEnumHttps),
		}

		_, err = elbv2client.CreateListener(createListenerInput)
		if err != nil {
			log.Println("Error creating ALB listener")
			log.Fatal(err)
			return events.ALBTargetGroupResponse{
				StatusCode: 503,
			}, err
		}

		log.Println("Successfully created listener")
	} else {
		log.Println("Dry run, not attempting to obtain certificate")
	}

	log.Println("Execution completed successfully")
	return events.ALBTargetGroupResponse{
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(DoACME)
}

func encodeECDSAKeyAsPem(key *ecdsa.PrivateKey) []byte {
	keyx509, err := x509.MarshalECPrivateKey(key)
	if err != nil {
		log.Println("Could not marshall private key")
		log.Fatal(err)
	}

	keyPem := pem.EncodeToMemory(&pem.Block{
		Type:  "ECDSA PRIVATE KEY",
		Bytes: keyx509,
	})

	return keyPem
}

func decodeECDSAKeyFromPem(pemKey []byte) *ecdsa.PrivateKey {
	pemBlock, _ := pem.Decode(pemKey)
	keyx509 := pemBlock.Bytes

	key, err := x509.ParseECPrivateKey(keyx509)
	if err != nil {
		log.Println("Could not unmarshall private key")
		log.Fatal(err)
	}

	return key
}

// Tries to get a private key used in an earlier invocation from AWS secretsmanager
// If it does, it will return the key and a true to indicate the user was already registered
// Otherwise it will create a new key, store it in secretsmanager and return false to indicate the user will not be registered
func getOrCreatePrivateKey(llcfg cfg.LambdaLegoConfig, sess *session.Session) (*ecdsa.PrivateKey, bool) {
	secretsmanagerSession := secretsmanager.New(sess)

	log.Println("Checking if key exists for lambda lego user")
	secretValueOutput, err := secretsmanagerSession.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(llcfg.GetACMEUserKeyID()),
	})

	var privateKey *ecdsa.PrivateKey
	userRegistered := false

	if err != nil {
		log.Println("No existing key for lambda lego user, creating key")
		newKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if err != nil {
			log.Println("Could not generate ecdsa key")
			log.Fatal(err)
		}

		keyPem := encodeECDSAKeyAsPem(newKey)

		log.Println("Uploading key to secretsmanager")
		_, err = secretsmanagerSession.CreateSecret(&secretsmanager.CreateSecretInput{
			Description:  aws.String("Key for lego-lambda ACME user"),
			Name:         aws.String(llcfg.GetACMEUserKeyID()),
			SecretBinary: keyPem,
		})

		if err != nil {
			log.Println("Unable to save key in secretsmanager")
			log.Fatal(err)
		}

		privateKey = newKey
	} else {
		keyPem := secretValueOutput.SecretBinary

		privateKey = decodeECDSAKeyFromPem(keyPem)
		userRegistered = true
	}

	return privateKey, userRegistered
}
